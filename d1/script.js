// let movies = ["Godfather", "Mission Impossible", "Avengers", "Venom", "The Nun"];
// // let movies = ["Godfather", "Mission Impossible", "Avengers", "Venom", "The Nun"];

// console.log(movies[0]); //Godfather
// console.log(movies [1]); //Mission Impossible
// console.log(movies[2]); //avengers

// movies [1] = "Halloween";

// console.log(movies[1]); //Halloween
// console.log(movies.length); //length is 5

// //We can initialize and empty array two ways:

// let colors = [];
// let colors = new Array() //uncommon

// //Arrays can hold any type of data

// let collection = [40, true , "hermione", null];

// //Arrays have a length property

// let nums = [45,37,89,24]
// nums.length //4

//Array Methods

// Push to add item in an array

// let colors = ["red", "orange", "yellow"];
// console.log(colors) // Output is ["red", "orange", "yellow"]

// colors.push("green");
// console.log(colors) // Output is ["red", "orange", "yellow", "green"]

//pop to remove last item

// let colors = ["red", "orange", "yellow"];
// colors.pop(); //removed yellow
// console.log(colors.pop()); //display what was removed

//use unshift to add to the front of an array

// let colors = ["red", "orange", "yellow"];
// colors.unshift("infrared");
// //["infrared", "red", "orange", "yellow"]

// console.log(colors);

// Use shift to remove the first item in an array

// let colors = ["red", "orange", "yellow"];
// console.log(colors.shift());
// //shift returns the removed item

// console.log(colors);

// Use indexOf() to find the index of an item in an array

// let tuitt = ["Charles", "Paul", "Sef", "Alex", "paul"];

// //returns the first index at which a given element can be found
// console.log(tuitt.indexOf("Sef")); //2

// //finds the first instance of Paul 1 not 4
// console.log(tuitt.indexOf("Paul")); //1

// //returns -1 if the element is not staement
// console.log(tuitt.indexOf("Hulk")); // -1

// // Array Iterations

// For Loop: To use the for loop we need to know length of array.

// let colors = ["red","orange", "yellow", "green"];

// for(let i = 0; i < colors.length; i++){
// 	console.log(colors[i]);
// }

// For each is a built in method for iterating over an array

let colors = ["red","orange", "yellow", "green"];

colors.forEach(function(color){
	console.log(color)		
})

//Anon
// let colors = ["red","orange", "yellow", "green"];

//color is a place holder
// colors.forEach(function(){

// });